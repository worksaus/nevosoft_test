<?php
namespace app\commands;

use app\models\dict\DictLogType;
use app\models\helpers\Model;
use app\models\Log;
use SplFileObject;
use yii\console\Controller;

class CommandController extends Controller
{
    public function actionImportLog($filename)
    {
        $file = new SplFileObject($filename);

        $lineCnt = 0;
        Log::deleteAll();
        while (!$file->eof() /*&& $lineCnt < 5*/) {
            $content = $file->fgets();
            $lineCnt++;
            $noticePos = strpos($content, 'NOTICE:');
            $warningPos = strpos($content, 'WARNING:');
            if ($noticePos === FALSE && $warningPos === FALSE) continue;
            $startTime = intval(strpos($content, '['))+1;
            $endTime = intval(strpos($content, ']'))-1;
            $time = strtotime(substr($content, $startTime, $endTime));
            if (!$time)  continue;
            if ($warningPos !== FALSE) {
                $message = trim(substr($content, $warningPos+8));
                $type = DictLogType::WARNING;
            } else {
                $message = trim(substr($content, $noticePos+7));
                $type = DictLogType::NOTICE;
            }

            $log = new Log();
            $log->message = $message;
            $log->ts = Date('Y-m-d H:i:s', $time);
            $log->type = $type;
            if (!$log->save()) {
                print_r(Model::getFirstError($log));
            }
        }

        $file = null;
    }
}

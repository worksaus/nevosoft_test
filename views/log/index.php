<?php

use app\models\dict\DictLogType;
use app\models\Log;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\LogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Логи';
//$this->params['breadcrumbs'][] = $this->title;
$typeList = DictLogType::getList();
?>
<div class="log-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'pager' => [
            'class' => '\yii\widgets\LinkPager',
            'pageCssClass' => 'page-item',
            'linkOptions' => ['class' => 'page-link'],
            'disabledListItemSubTagOptions' => ['class' => 'page-link'],
            'disabledPageCssClass' => 'page-item',
            'firstPageLabel' => '<<',
            'lastPageLabel'  => '>>',
            'prevPageLabel' => '<',
            'nextPageLabel' => '>',
        ],
        'columns' => [
            'id',
            [
                'attribute' => 'ts',
                'label'     => 'Дата создания',
                'format'    => ['date', 'php:d.m.Y H:i:s'],
                'value'     => 'ts',
            ],
            [
                'label' => 'Тип',
                'attribute' => 'type',
                'value'     => function (Log $model) use ($typeList) {
                    return $typeList[$model->type] ?? $model->type;
                },
                'filter'    => $typeList

            ],
            [
                'attribute' => 'message',
                'label' => 'Сообщение'
            ],
        ],
    ]); ?>


</div>

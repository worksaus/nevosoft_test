DIRECTORY STRUCTURE
-------------------

      assets/             contains assets definition
      commands/           contains console commands (controllers)
      config/             contains application configurations
      controllers/        contains Web controller classes
      mail/               contains view files for e-mails
      models/             contains model classes
      runtime/            contains files generated during runtime
      tests/              contains various tests for the basic application
      vendor/             contains dependent 3rd-party packages
      views/              contains view files for the Web application
      web/                contains the entry script and Web resources



REQUIREMENTS
------------

The minimum requirement by this project template that your Web server supports PHP 5.6.0.


INSTALLATION
------------

1. In project folder run "composer install" command
2. Create mysql database and edit config/db-local.php file
3. Import nevosoft_dmp.gz dump if you want to get database filled in. Run migrations otherwise ( "php yii migrate" command )
4. You can also import data from log.error example file by running "php yii command/import-log" command

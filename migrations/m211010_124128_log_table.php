<?php

use yii\db\Migration;

/**
 * Class m211010_124128_log_table
 */
class m211010_124128_log_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $connection = Yii::$app->getDb();
        $connection->createCommand("
            create table log
            (
                id int unsigned auto_increment primary key,
                ts timestamp default current_timestamp() not null,
                type tinyint unsigned default 1 not null,
                message text null
            );
        ")->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m211010_124128_log_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211010_124128_log_table cannot be reverted.\n";

        return false;
    }
    */
}

<?php
/**
 * User: ansaus
 * Date: 11.09.2019
 */

namespace app\models\helpers;


class Model
{
    public static function getFirstError(\yii\base\Model $model) {
        return (count($model->getFirstErrors())) ? array_values($model->getFirstErrors())[0] : '';
    }
}

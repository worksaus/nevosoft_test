<?php
/**
 * User: ansaus
 * Date: 09.10.2021
 */

namespace app\models\dict;

class DictLogType
{
    const WARNING = 1;
    const NOTICE = 2;

    public static function getList()
    {
        return [
            self::WARNING => 'Warning',
            self::NOTICE => 'Notice',
        ];
    }
}
